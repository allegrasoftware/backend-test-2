package mx.com.walmart.OrderComments.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mx.com.walmart.OrderComments.entity.Commentary;
import mx.com.walmart.OrderComments.entity.Item;
import mx.com.walmart.OrderComments.entity.ItemPK;
import mx.com.walmart.OrderComments.entity.OrderMarket;
import mx.com.walmart.OrderComments.service.CommentItemService;
import mx.com.walmart.OrderComments.service.CommentService;

@RestController
@RequestMapping("comments")
@Validated
public class CommentController 
{
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private CommentItemService commentItemService;

	@RequestMapping(value= {"feedbacks","feedbacksByRating/{filterQualification}"}, method = RequestMethod.GET)
	public ResponseEntity<Page<Commentary>> readComments(@PageableDefault(sort = {"registrationDate"}, direction = Sort.Direction.DESC, value = 20) final Pageable pageable, @PathVariable(required = false) String filterQualification)
	{
		return ResponseEntity.ok(commentService.readComments(pageable, filterQualification));
	}
	
	@RequestMapping(value= "{idCommentary}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Commentary>> readCommentById(@PathVariable("idCommentary") Integer idCommentary)
	{
		return ResponseEntity.ok(commentService.readCommentById(idCommentary));
	}
	
	
	@PostMapping(value="{idOrderMarket}")
	public ResponseEntity<?> createComment(@Valid @PathVariable("idOrderMarket") Integer idOrderMarket, @RequestBody Commentary commentary)
	{
		if(commentService.readOrderMarketById(idOrderMarket)!=null)
		{
			return new ResponseEntity<>("The idOrderMarket already has a feedback",HttpStatus.BAD_REQUEST);
		}
		commentary.setOrderMarket(new OrderMarket(idOrderMarket));
		return ResponseEntity.ok(commentService.createComment(commentary));
	}
	
	
	@DeleteMapping(value="{idCommentary}")
	public ResponseEntity<Void> deleteComment(@PathVariable("idCommentary") Integer idCommentary)
	{
		commentService.deleteComment(idCommentary);
		return ResponseEntity.ok(null);
	}
	
	
	@PatchMapping(value="{idCommentary}")
	public ResponseEntity<Commentary> editComment(@PathVariable("idCommentary") Integer idCommentary, @RequestBody Map<String, Object> changesInCommentary)
	{	
        Commentary commentary=commentService.UpdateComment(idCommentary, changesInCommentary);
        return ResponseEntity.ok(commentary);
	}
	
	@PutMapping(value="feedbackItem/{idOrderMarket}/{idcatItem}")
	public ResponseEntity<Item> createCommentItem(@PathVariable("idOrderMarket") Integer idOrderMarket,@PathVariable("idcatItem") Integer idcatItem, @RequestBody Item item)
	{
		ItemPK itemPK = new ItemPK(idOrderMarket, idcatItem);
		item.setItemPK(itemPK);
		return ResponseEntity.ok(commentItemService.createCommentItem(item));
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleValidationExceptions(ConstraintViolationException ex) {
	    
		Map<String, String> errors = new HashMap<>();
		
	    ex.getConstraintViolations().forEach((error) -> 
	    {
	        Path fieldName = error.getPropertyPath();
	        String errorMessage = error.getMessage();
	        errors.put(fieldName.toString(), errorMessage);
	    });
	    
	    return errors;
	}
}
