package mx.com.walmart.OrderComments.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AddUserHeaderFilter implements Filter {

	private static final String ID_USUARIO_LOGUEADO_GABRIELA="1";
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			 throws IOException, ServletException {

		HttpServletResponse  myResponse= (HttpServletResponse) response;
		myResponse.addHeader("UserId", ID_USUARIO_LOGUEADO_GABRIELA);
		
		filterChain.doFilter(request, myResponse);

	}

}