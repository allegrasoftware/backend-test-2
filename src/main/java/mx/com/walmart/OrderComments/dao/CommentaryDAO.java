package mx.com.walmart.OrderComments.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mx.com.walmart.OrderComments.entity.Commentary;

@Repository
public interface CommentaryDAO extends JpaRepository<Commentary, Integer> {

	@Query("SELECT c FROM Commentary c where qualification=?1")
    public Page<Commentary> findFilterQualification(Integer filterQualification, Pageable pageable);
	
	@Query("SELECT c FROM Commentary c where id_order_market=?1")
    public Commentary findOrderMarket(Integer idOrderMarket);
}
