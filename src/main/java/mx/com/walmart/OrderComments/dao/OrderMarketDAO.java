package mx.com.walmart.OrderComments.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.walmart.OrderComments.entity.OrderMarket;

@Repository
public interface OrderMarketDAO extends JpaRepository<OrderMarket, Integer>{

}
