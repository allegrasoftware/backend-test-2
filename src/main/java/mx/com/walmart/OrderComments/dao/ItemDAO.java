package mx.com.walmart.OrderComments.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.walmart.OrderComments.entity.Item;
import mx.com.walmart.OrderComments.entity.ItemPK;

@Repository
public interface ItemDAO extends JpaRepository<Item, ItemPK>{

}
