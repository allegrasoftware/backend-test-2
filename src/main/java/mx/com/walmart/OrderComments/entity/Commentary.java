package mx.com.walmart.OrderComments.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="commentary")
public class Commentary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCommentary")
	private int idCommentary;

	@NotNull(message = "Commentary is mandatory")
	@Column(name = "commentary")
	private String commentary;

	@NotNull(message = "Qualification is mandatory")
	@Min(1)
	@Max(5)
	@Column(name="qualification")
    private Integer qualification;	
	
	@NotNull(message = "RegistrationDate is mandatory")
	@Column(name="registrationDate")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Timestamp registrationDate;

	
	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "idOrderMarket", referencedColumnName = "idOrderMarket")
	private OrderMarket orderMarket;

	public Commentary() {}

	public Commentary(int idCommentary) {
		this.idCommentary = idCommentary;
	}
	
	public Commentary(int idCommentary, String commentary, Integer qualification, OrderMarket orderMarket, Timestamp registrationDate) {
	        this.idCommentary = idCommentary;
	        this.commentary = commentary;
	        this.qualification = qualification;
	        this.orderMarket = orderMarket;
	        this.registrationDate=registrationDate; 
	    }

	public int getIdCommentary() {
		return idCommentary;
	}

	public void setIdCommentary(int idCommentary) {
		this.idCommentary = idCommentary;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public OrderMarket getOrderMarket() {
		return orderMarket;
	}

	public void setOrderMarket(OrderMarket orderMarket) {
		this.orderMarket = orderMarket;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Integer getQualification() {
		return qualification;
	}

	public void setQualification(Integer qualification) {
		this.qualification = qualification;
	}
	
}
