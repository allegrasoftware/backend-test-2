package mx.com.walmart.OrderComments.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="orderMarket")
public class OrderMarket {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idOrderMarket")
	private int idOrderMarket;

	@Column(name = "store")
	private String store;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idUser", referencedColumnName = "idUser")
    private User user;

	public OrderMarket() {}
	
	public OrderMarket(int idOrderMarket) {
		this.idOrderMarket = idOrderMarket;
	}

	public OrderMarket(int idOrderMarket, String store, User user) {
	        this.idOrderMarket = idOrderMarket;
	        this.store = store;
	        this.user = user;
	    }

	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getIdOrderMarket() {
		return idOrderMarket;
	}

	public void setIdOrderMarket(int idOrderMarket) {
		this.idOrderMarket = idOrderMarket;
	}

}
