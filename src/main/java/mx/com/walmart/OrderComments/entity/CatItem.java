package mx.com.walmart.OrderComments.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="catitem")
public class CatItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idcatItem")
	private int idcatItem;

	@Column(name = "description")
	private String description;

	public CatItem() {
		
	}
	
	public CatItem(int idcatItem, String description) {
        this.idcatItem = idcatItem;
        this.description = description;
       
    }
	
	public int getIdcatItem() {
		return idcatItem;
	}

	public void setIdcatItem(int idcatItem) {
		this.idcatItem = idcatItem;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
