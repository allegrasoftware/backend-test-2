package mx.com.walmart.OrderComments.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item {

    @EmbeddedId
    private ItemPK itemPK;

	@Column(name = "commentary")
	private String commentary;

	public Item() {}
	
	public Item(ItemPK itemPK, String commentary) {
		this.itemPK=itemPK;
		this.commentary=commentary;
		
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public ItemPK getItemPK() {
		return itemPK;
	}

	public void setItemPK(ItemPK itemPK) {
		this.itemPK = itemPK;
	}


	
}
