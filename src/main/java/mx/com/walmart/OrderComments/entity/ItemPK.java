package mx.com.walmart.OrderComments.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Embeddable
public class ItemPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "idcatItem", referencedColumnName = "idcatItem")
	private CatItem catItem;
	
	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "idOrderMarket", referencedColumnName = "idOrderMarket")
	private OrderMarket orderMarket;

	public ItemPK() {
		
	}
	
	public ItemPK(Integer idOrderMarket, Integer idcatItem) {
		
		this.catItem=new CatItem();
		this.orderMarket=new OrderMarket();
		this.catItem.setIdcatItem(idcatItem);
		this.orderMarket.setIdOrderMarket(idOrderMarket);
	}
	
	public CatItem getCatItem() {
		return catItem;
	}

	public void setCatItem(CatItem catItem) {
		this.catItem = catItem;
	}

	public OrderMarket getOrderMarket() {
		return orderMarket;
	}

	public void setOrderMarket(OrderMarket orderMarket) {
		this.orderMarket = orderMarket;
	}
	
	
}
