package mx.com.walmart.OrderComments.utilities;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {

	public static Timestamp stringToTimestamp(String dateFormat) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = null;
		Timestamp registrationDate = null;
		try {
			
			date = simpleDateFormat.parse(dateFormat);
			registrationDate = new Timestamp(date.getTime());
			
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return registrationDate;
	}

}
