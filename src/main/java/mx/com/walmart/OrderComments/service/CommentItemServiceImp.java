package mx.com.walmart.OrderComments.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

import mx.com.walmart.OrderComments.dao.ItemDAO;
import mx.com.walmart.OrderComments.entity.Item;

@Service
@SpringBootApplication(scanBasePackages = {"mx.com.walmart.OrderComments.dao"})
public class CommentItemServiceImp implements CommentItemService {

	@Autowired
	private ItemDAO itemDAO;
	
	@Override
	public Item createCommentItem(Item item) {
		
		return itemDAO.save(item);
	}

}
