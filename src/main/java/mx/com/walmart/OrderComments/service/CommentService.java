package mx.com.walmart.OrderComments.service;

import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.com.walmart.OrderComments.entity.Commentary;

public interface CommentService {

	Page<Commentary> readComments(Pageable pageable, String filterQualification);
	
	Optional<Commentary> readCommentById(Integer idCommentary);
	
	Commentary createComment(Commentary commentary);
	
	Commentary readOrderMarketById(Integer idOrderMarket);
	
	Commentary UpdateComment(Integer idCommentary, Map<String, Object> changesInCommentary);
	
	void deleteComment(Integer idCommentary);
	
	
}
