package mx.com.walmart.OrderComments.service;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mx.com.walmart.OrderComments.dao.CommentaryDAO;
import mx.com.walmart.OrderComments.entity.Commentary;

@Service
@SpringBootApplication(scanBasePackages = {"mx.com.walmart.OrderComments.dao"})
public class CommentServiceImp implements CommentService {

	@Autowired
	private CommentaryDAO commentaryDAO;

	@Override
	public Page<Commentary> readComments(Pageable pageable, String filterQualification) {

		if (filterQualification != null) {
			Integer filterQueryQualification=Integer.parseInt(filterQualification);
			return (Page<Commentary>) commentaryDAO.findFilterQualification(filterQueryQualification,pageable);
		}
		return commentaryDAO.findAll(pageable);

	}

	@Override
	public Optional<Commentary> readCommentById(Integer idCommentary) {
		
		return commentaryDAO.findById(idCommentary);
	}

	@Override
	public Commentary createComment(Commentary commentary) {
		
		return commentaryDAO.save(commentary);
	}
	
	@Override
	public Commentary UpdateComment(Integer idCommentary, Map<String, Object> changesInCommentary) {

		Optional<Commentary> commentaryOptional = commentaryDAO.findById(idCommentary);
		Commentary commentaryUpdate = commentaryOptional.get();

		changesInCommentary.forEach((change, value) -> {
			switch (change) {
			case "commentary":
				commentaryUpdate.setCommentary((String) value);
				break;
			case "qualification":
				commentaryUpdate.setQualification((Integer) value);
				break;
			}
		});

		return commentaryDAO.save(commentaryUpdate);
	}

	@Override
	public void deleteComment(Integer idCommentary) {
		
		commentaryDAO.deleteById(idCommentary);
	}

	@Override
	public Commentary readOrderMarketById(Integer idOrderMarket) {

		return commentaryDAO.findOrderMarket(idOrderMarket);
	}
	
}
