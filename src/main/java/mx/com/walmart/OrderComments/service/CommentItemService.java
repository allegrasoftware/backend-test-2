package mx.com.walmart.OrderComments.service;

import mx.com.walmart.OrderComments.entity.Item;

public interface CommentItemService {

	Item createCommentItem(Item item);
}
