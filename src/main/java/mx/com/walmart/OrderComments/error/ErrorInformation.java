package mx.com.walmart.OrderComments.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorInformation extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("message")
	private String message;
	@JsonProperty("status_code")
	private int statusCode;
	@JsonProperty("uri")
	private String uriRequested;

	public ErrorInformation(String message, Throwable cause) {
		super(message, cause);
	}

	public ErrorInformation(String message) {
		super(message);
	}

	public ErrorInformation(Throwable cause) {
		super(cause);
	}

	public ErrorInformation(int statusCode, String message, String uriRequested) {
		this.message = message;
		this.statusCode = statusCode;
		this.uriRequested = uriRequested;
	}

	public String getMessage() {
		return message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getUriRequested() {
		return uriRequested;
	}
}
