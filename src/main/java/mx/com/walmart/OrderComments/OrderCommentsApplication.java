package mx.com.walmart.OrderComments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.validation.annotation.Validated;

@SpringBootApplication()
@ComponentScan({"mx.com.walmart.OrderComments"})
@EntityScan("mx.com.walmart.OrderComments.entity")
@ServletComponentScan("mx.com.walmart.OrderComments.filter")
@Validated
public class OrderCommentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderCommentsApplication.class, args);
	}

}
