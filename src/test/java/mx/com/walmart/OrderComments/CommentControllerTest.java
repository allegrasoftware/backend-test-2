package mx.com.walmart.OrderComments;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import mx.com.walmart.OrderComments.controller.CommentController;
import mx.com.walmart.OrderComments.dao.CommentaryDAO;
import mx.com.walmart.OrderComments.dao.ItemDAO;
import mx.com.walmart.OrderComments.entity.Commentary;
import mx.com.walmart.OrderComments.entity.Item;
import mx.com.walmart.OrderComments.entity.ItemPK;
import mx.com.walmart.OrderComments.entity.OrderMarket;
import mx.com.walmart.OrderComments.utilities.Utilities;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
public class CommentControllerTest {
	
	@MockBean
    private CommentaryDAO commentaryDao;
	
	@MockBean
    private ItemDAO itemDao;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void readCommentsAPI() throws Exception {

        Mockito.when(commentaryDao.findAll())
                .thenReturn(
                        Lists.newArrayList(
                                new Commentary(0, "", 0, null, null),
                                new Commentary(1, "", 0, null, null)
                        )
                );

        mockMvc.perform(MockMvcRequestBuilders
                .get("/comments/feedbacks")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        
    }
    
    @Test
    public void readCommentByIdAPI() throws Exception {

        Mockito.when(commentaryDao.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(new Commentary(1, "Comentario 1", 0, null, null)));

        mockMvc.perform(MockMvcRequestBuilders
                .get("/comments/1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("idCommentary").isNotEmpty())
                .andExpect(jsonPath("$.commentary", is("Comentario 1")));
    }
    
    @Test
    public void createCommentAPI() throws Exception {

    	Timestamp registrationDate=Utilities.stringToTimestamp("2021-03-09 05:30:00");	
	    Commentary commentary = new Commentary(4, "Comentario 1", 5, new OrderMarket(2), registrationDate);	    
	    String jsonInString = "{\"idCommentary\":4,\"commentary\":\"Comentario 1\",\"qualification\":5,\"registrationDate\":\"2021-03-09T05:30:00.000Z\",\"orderMarket\":{\"idOrderMarket\":2}}";
	    
	    Mockito.when(commentaryDao.findById(Mockito.anyInt()))
        .thenReturn(Optional.of(commentary));
	    
	    Mockito.when(commentaryDao.save(Mockito.any(Commentary.class))).thenReturn(commentary);
	    
	
	    RequestBuilder requestBuilder = MockMvcRequestBuilders
					.post("/comments/2")
					.accept(MediaType.APPLICATION_JSON).content(jsonInString)
					.contentType(MediaType.APPLICATION_JSON);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		assertEquals(HttpStatus.OK.value(), response.getStatus());

         
    }
    
    @Test
    public void editCommentAPI() throws Exception {

 	    Timestamp registrationDate=Utilities.stringToTimestamp("2021-03-09 05:30:00");	
 	    Commentary commentary = new Commentary(4, "Comentario 1", 5, new OrderMarket(2), registrationDate);
 	    
 	    Mockito.when(commentaryDao.save(Mockito.any(Commentary.class))).thenReturn(commentary);
 	     	
    }
    
    @Test
    public void deleteCommentAPI() throws Exception {

    	doNothing().when(commentaryDao).deleteById(1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
					.delete("/comments/1")
					.contentType(MediaType.APPLICATION_JSON);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		assertEquals(HttpStatus.OK.value(), response.getStatus());
  
    }
    
    @Test
    public void createCommentItemAPI() throws Exception {

    	ItemPK itemPK = new ItemPK(1, 2);
	    Item item = new Item(itemPK,"Comentario 1");	    
	    String jsonInString = "{\"commentary\":\"Comentario 1\"}";

	    
	    Mockito.when(itemDao.save(Mockito.any(Item.class))).thenReturn(item);
	    
	
	    RequestBuilder requestBuilder = MockMvcRequestBuilders
					.put("/comments/feedbackItem/1/2")
					.accept(MediaType.APPLICATION_JSON).content(jsonInString)
					.contentType(MediaType.APPLICATION_JSON);
	
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
	
		assertEquals(HttpStatus.OK.value(), response.getStatus());

         
    }

}
